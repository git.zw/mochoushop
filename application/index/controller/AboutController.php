<?php
namespace app\index\controller;
// namespace app\zw_admin\controller;
use \think\Controller;
use think\Db;
// use think\Controller;
use think\Session;
use think\Request;


class AboutController extends Controller
{

	/**
	 * 公司简介
	 * @return [type] [description]
	 */
    public function index()
    {
		
		return $this->fetch('index');
    }


    /**
     * 联系我们
     * @return [type] [description]
     */
    public function contact()
    {
    	return $this->fetch('contact');
    }

    /**
     * 我们的优势
     * @return [type] [description]
     */
    public function actor()
    {
    	return $this->fetch('actor');
    }

    /**
     * 招贤纳士
     * @return [type] [description]
     */
    public function recruit()
    {
    	return $this->fetch('recruit');
    }


    /**
     * 新闻资讯
     * @return [type] [description]
     */
    public function new_list()
    {
    	// return $this->fetch('new_list');
    	return $this->fetch('new_list');
    }
}




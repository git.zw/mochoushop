<?php
namespace app\index\controller;
// namespace app\zw_admin\controller;
use \think\Controller;
use think\Db;
// use think\Controller;
use think\Session;
use think\Request;


class BottomController extends Controller
{
	/**
	 * 订购流程
	 * @return [type] [description]
	 */
    public function process()
    {
		
		return $this->fetch('process');
    }


    /**
     * 品牌保障
     * @return [type] [description]
     */
    public function brand_title()
    {	
    	return $this->fetch('brand_title');
    }


    /**
     * 注册协议
     * @return [type] [description]
     */
    public function register()
    {
    	return $this->fetch('register');
    }


    /**
     * 隐私保护
     * @return [type] [description]
     */
    public function privacy()
    {
    	return $this->fetch();
    }


    /**
     * 验收签收
     * @return [type] [description]
     */
    public function sign()
    {
    	return $this->fetch();
    } 




    /**
     * 交货方式
     * @return [type] [description]
     */
    public function mode()
    {
    	return $this->fetch();
    }


    /**
     * 退换说明
     * @return [type] [description]
     */
    public function explain()
    {
    	return $this->fetch();
    }
}

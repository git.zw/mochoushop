<?php
namespace app\index\controller;
// namespace app\zw_admin\controller;
use \think\Controller;
use think\Db;
// use think\Controller;
use think\Session;
use think\Request;


class CoreController extends Controller
{



    public function  _initialize()
    {
       // echo '111';
       // exit;
        $uid = Session::get('member_id');
        $sid = Session::get('seller_id');
        // echo $sid;
        // exit;
        // dump($_SESSION);
        // exit;
        if (empty($uid) && empty($sid)) {
            return  $this->error('您还没有登入,请先登入!');
        }
        // echo $request->session('name'); 
        // dump($_SESSION);
        // exit;
        // return $request->session('user_name');
        // echo Session::get('uid');
    }

    



    public function index()
    {
		  $uid = Session::get('member_id');
      $user_list = Db::name('member')->where(['uid'=>$uid])->find();
      $this->assign('phone',$user_list['phone']);
		  return $this->fetch('index');
    }



    /**
     * 我的积分
     * @return [type] [description]
     */
    public function integral()
    {
      return $this->fetch();
    }


    /**
     * 账户信息
     * @return [type] [description]
     */
    public function account()
    {
      return $this->fetch();
    }


    /**
     * 账户安全
     * @return [type] [description]
     */
    public function security()
    {
      return $this->fetch();
    }


    /**
     * 晒家相册
     * @return [type] [description]
     */
    public function home()
    {
      return $this->fetch();
    }
}

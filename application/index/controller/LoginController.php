<?php
namespace app\index\controller;
// namespace app\zw_admin\controller;
use \think\Controller;
use think\Db;
// use think\Controller;
use think\Session;
use think\Request;
use alisms\SmsDemo;

class LoginController extends Controller
{
    public function login()
    {
		
		return $this->fetch('login');
    }



    /**
     * 用户注册
     * @return [type] [description]
     */
    public function register()
    {
    	// dump(input('post.'));
    	$vas = input('post.code');
    	$phone = input('post.phone');
    	$pwd = input('post.pwd');

    	$vas_list = Db::name('verification')->where(['phone'=>$phone])->find();
    	// dump($vas_list);
    	if ($vas != $vas_list['vas']) {
    		//如果验证码不正确 ，返回false;
    		echo '3';
    		exit;
    	}

    	$list['pwd'] = md5(md5($pwd));
    	$list['phone'] = $phone;
    	$list['time'] = date('Y-m-d H:i');
    	if (db('member')->insert($list)) {
    		echo '1';
    		exit;
    	}else{
    		echo '2';
    		exit;
    	}


    }

    /**
     * 短信验证
     * @return [type] [description]
     */
    public function verification()
    {
    	// include(ROOT_PATH."extend/aliyun-dysms-php-sdk/api_demo/smsdemo.php");
    	include_once ROOT_PATH.'extend/aliyun-dysms-php-sdk/api_demo/smsdemo.php';
    	// dump(input('post.'));
    	$phone = input('post.phone');
		$TemplateCode = 'SMS_127153628';
		$Verification_one = rand(1000,9999);
		$Verification_one = (string)$Verification_one;
		$phone_list = Db::name('member')->where(['phone'=>$phone])->find();
        //手机号码验证
		if (!empty($phone_list)) {
			
			echo '5' ;
			exit;
		}

		$vas_phone = Db::name('verification')->where(['phone'=>$phone])->find();
		// dump($vas_phone);
		// exit;
		if ($vas_phone['num'] >= 5) {
			echo '4';
			exit;
		}
        if (empty($vas_phone)) {
			$list['ip']    = $_SERVER["REMOTE_ADDR"];
			$list['phone'] = $_POST['phone'];
			$list['vas']   = $Verification_one;
			$list['time']  = date('Y-m-d H:i');
			$list['num']   = '1';
			// $insert_array[] = $list;
			// dump($insert_array);
			// exit;
			db('verification')->insert($list);
			// echo '111';
			// exit;
        }else if($vas_phone['num'] < '5'){
	        	$list['vas']    = $Verification_one;
	        	$list['time']   = date('Y-m-d H:i');
	        	$list['num']  = $vas_phone['num'] + 1;
	        	db('verification')->where(array('phone'=>$phone))->update($list);
	        	
        }else{
        	echo '3';
        	exit;
        }

        $Verification = $Verification_one;
		$response = SmsDemo::sendSms($phone,$TemplateCode,$Verification);
		if ($response) {
			echo '1';
		}else{
			echo '2';
		}
		exit;
    }



    /**
     * 登入操作
     * @return [type] [description]
     */
    public function login_add()
    {
    	// dump(input('post.'));
    	// dump($_SESSING);
    	// $vas = input('post.code');
    	$phone = input('post.phone');
    	$pwd = md5(md5(input('post.pwd')));
    	$member_list = Db::name('member')->where(['phone'=>$phone])->find();
    	$seller_list = Db::name('seller')->where(['username'=>$phone])->find();

    	if (empty($member_list) && empty($seller_list)) {
    		echo 2;
    		exit;
    	}else if(!empty($member_list) && $phone == $member_list['phone'] && $pwd == $member_list['pwd']){
    		Session::set('member_id',$member_list['uid']);
    		Session::set('member_phone',$member_list['phone']);
      		echo 1;
      		exit;
    	}else if(!empty($seller_list) && $phone == $seller_list['username'] && $pwd == $seller_list['pwd']){
    		// echo '13232';
    		Session::set('seller_id',$seller_list['seller']);
    		Session::set('seller_name',$seller_list['username']);
      		echo 1;
      		exit;
    	}else{
    		echo 3;
    		exit;
    	}


    	// dump($member_list);
    	// echo $member_list['phone'];
    	// echo $phone;
    	// if ($phone == $member_list['phone'] && $pwd == $member_list['pwd']) {
    		// Session::set('member_id',$member_list['uid']);
    		// Session::set('member_phone',$member_list['phone']);
    		// // echo Session::get('member_id');        
      // //   	echo Session::get('member_phone');
      // 		echo 1;
      // 		exit;

    	// }else{
    	// 	echo 2;
    	// 	exit;
    	// }

    }



    /**
     * 退出登入
     * @return [type] [description]
     */
    public function delselling()
    {
    	session(null);
    	echo 1;
    	exit;
    }
}

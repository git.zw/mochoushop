<?php
/**
 * Created by PhpStorm.
 * User: liyu
 * Date: 2018/1/23
 * Time: 下午3:12
 */

namespace app\mobile\controller\v1;


use app\mobile\service\UserService;
use app\mobile\validate\GetUserProfitDetailValidate;
use app\mobile\validate\GetUserProfitValidate;
use app\mobile\validate\loginValidate;
use think\Cache;
use think\Request;

class UserController extends BaseController
{
    public function login()
    {
        (new loginValidate())->goCheck();
        $data = Request::instance()->only(['phone','password'],'post');
        $result = (new UserService())->login($data);
        return $this->ajaxReturn($result);
    }

    public function getUserInfo()
    {
        $result = (new UserService())->getUserInfo();
        return $this->ajaxReturn($result);
    }

    public function getUserProfit()
    {
        (new GetUserProfitValidate())->goCheck();
        $data = Request::instance()->only(['startTime','endTime'],'get');
        $result = (new UserService())->getUserProfit($data);
        return $this->ajaxReturn($result);
    }

    public function getUserProfitDetail()
    {
        (new GetUserProfitDetailValidate())->goCheck();
        $page = input('get.page');
        $data = Request::instance()->only(['startTime','endTime'],'get');
        $result = (new UserService())->getUserProfitDetail($data,$page);
        return $this->ajaxReturn($result);
    }
}
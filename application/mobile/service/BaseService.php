<?php
/**
 * Created by PhpStorm.
 * User: liyu
 * Date: 2018/1/23
 * Time: 下午4:54
 */

namespace app\mobile\service;


class BaseService
{
    protected function db_error_check($row)
    {
        if ($row < 0) {
            set_error_exception('db_error');
        }
    }
}
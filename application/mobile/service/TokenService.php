<?php
/**
 * Created by PhpStorm.
 * User: liyu
 * Date: 2018/1/23
 * Time: 下午5:55
 */

namespace app\mobile\service;



use think\Cache;

class TokenService extends BaseService
{
    protected $user_id;

    protected static $instance;

    protected function __construct()
    {

    }

    public static function instance()
    {
        if (empty(self::$instance)) {
            self::$instance = new static();
        }

        return self::$instance;
    }

    public function createToken($data)
    {
        $token = md5($data['phone'].microtime(true));
        if (!Cache::set($token,$data)) {
            set_error_exception('redis_cache_fail');
        }

        return $token;
    }

    protected function getUserInfo($token)
    {
        $result = Cache::get($token);
        if ($result === false) {
            set_error_exception('token_error');
        }
        return $result;
    }

    public function setUserId($token)
    {
        $user = $this->getUserInfo($token);
        $this->user_id = $user['id'];
    }

    public function getUserId()
    {
        return $this->user_id;
    }
}
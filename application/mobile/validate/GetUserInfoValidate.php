<?php
/**
 * Created by PhpStorm.
 * User: liyu
 * Date: 2018/1/23
 * Time: 下午6:42
 */

namespace app\mobile\validate;


use validate\BaseValidate;

class GetUserInfoValidate extends BaseValidate
{
    protected $rule = [
        'token' => 'require'
    ];
}
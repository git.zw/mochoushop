<?php
/**
 * Created by PhpStorm.
 * User: liyu
 * Date: 2018/1/25
 * Time: 上午11:55
 */

namespace app\mobile\validate;


use validate\BaseValidate;

class GetUserProfitDetailValidate extends BaseValidate
{
    protected $rule = [
        'page' => 'require',
        'startTime' => 'require',
        'endTime' => 'require'
    ];
}
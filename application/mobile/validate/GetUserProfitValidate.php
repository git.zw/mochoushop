<?php
/**
 * Created by PhpStorm.
 * User: liyu
 * Date: 2018/1/24
 * Time: 下午1:43
 */

namespace app\mobile\validate;


use validate\BaseValidate;

class GetUserProfitValidate extends BaseValidate
{
    protected $rule = [
        'startTime' => 'require',
        'endTime' => 'require'
    ];
}
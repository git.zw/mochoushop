<?php
/**
 * Created by PhpStorm.
 * User: liyu
 * Date: 2018/1/24
 * Time: 上午11:01
 */

namespace app\mobile\validate;


use validate\BaseValidate;

class TokenValidate extends BaseValidate
{
    protected $rule = [
        'token' => 'require'
    ];
}
<?php
/**
 * Created by PhpStorm.
 * User: liyu
 * Date: 2018/1/23
 * Time: 下午5:35
 */

namespace app\mobile\validate;


use validate\BaseValidate;

class loginValidate extends BaseValidate
{
    protected $rule = [
        'phone' => 'require',
        'password' => 'require'
    ];
}
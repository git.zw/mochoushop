<?php
/**
 * Created by PhpStorm.
 * User: liyu
 * Date: 2018/1/17
 * Time: 下午6:20
 */

namespace app\model;


use think\Model;

class Base extends Model
{
    protected $resultSetType = 'collection';
}
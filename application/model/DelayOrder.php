<?php
/**
 * Created by PhpStorm.
 * User: liyu
 * Date: 2018/1/18
 * Time: 下午6:32
 */

namespace app\model;


class DelayOrder extends Base
{
    protected $autoWriteTimestamp = 'datetime';

    public static function getListByWhere($where)
    {
        return self::all(function($query)use($where){
            return $query->where($where);
        });
    }
}
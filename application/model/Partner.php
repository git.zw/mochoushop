<?php
/**
 * Created by PhpStorm.
 * User: liyu
 * Date: 2018/1/23
 * Time: 下午1:06
 */

namespace app\model;


class Partner extends Base
{
    protected $autoWriteTimestamp = 'datetime';

    public function setPasswordAttr($value)
    {
        return md5($value);
    }

    public function setIdNumImagePositiveAttr($value)
    {
        return parse_url($value,PHP_URL_PATH);
    }

    public function getIdNumImagePositiveAttr($value)
    {
        if(empty($value))
            return '';
        return 'http://'.$_SERVER['HTTP_HOST'].$value;
    }

    public function setIdNumImageReverseAttr($value)
    {
        return parse_url($value,PHP_URL_PATH);
    }

    public function getIdNumImageReverseAttr($value)
    {
        if(empty($value))
            return '';
        return 'http://'.$_SERVER['HTTP_HOST'].$value;
    }

    public function setBankCardImageAttr($value)
    {
        return parse_url($value,PHP_URL_PATH);
    }

    public function getBankCardImageAttr($value)
    {
        if(empty($value))
            return '';
        return 'http://'.$_SERVER['HTTP_HOST'].$value;
    }

    public static function addOne($data)
    {
        $result = self::create($data,['name','password','id_num','phone','bank_card','id_num_image_positive','id_num_image_reverse','bank_card_image','entry_time']);
        return $result;
    }

    public static function updateOne($data,$where)
    {
        $row = self::update($data,$where,['name','password','id_num','phone','is_agreement','status','bank_card','id_num_image_positive','id_num_image_reverse','bank_card_image','entry_time','departure_time']);
        return $row;
    }

    public static function getListByPageWhere($page,$where)
    {
        $result = self::all(function($query)use($page,$where){
            return $query->where($where)->page($page,config('common.page_20'));
        });
        return $result;
    }

    public static function getOneByWhere($where)
    {
        $result = self::get(function($query)use($where){
            return $query->where($where);
        });
        return $result;
    }
}
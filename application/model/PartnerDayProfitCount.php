<?php
/**
 * Created by PhpStorm.
 * User: liyu
 * Date: 2018/1/19
 * Time: 下午12:39
 */

namespace app\model;


class PartnerDayProfitCount extends Base
{
    protected $autoWriteTimestamp = 'datetime';

    public static function getListByWhere($where)
    {
        return self::all(function($query)use($where){
            return $query->where($where);
        });
    }
}
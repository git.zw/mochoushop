<?php
/**
 * Created by PhpStorm.
 * User: liyu
 * Date: 2018/1/17
 * Time: 下午6:19
 */

namespace app\model;


class PartnerProfit extends Base
{
    protected $autoWriteTimestamp = 'datetime';

    public static function getListByWhere($where)
    {
        return self::all(function($query)use($where){
            return $query->where($where);
        });
    }
}
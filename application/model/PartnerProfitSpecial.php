<?php
/**
 * Created by PhpStorm.
 * User: liyu
 * Date: 2018/1/17
 * Time: 下午7:17
 */

namespace app\model;


class PartnerProfitSpecial extends Base
{
    protected $autoWriteTimestamp = 'datetime';

    public static function getListByWhere($where)
    {
        return self::all(function($query)use($where){
            return $query->where($where);
        });
    }
}
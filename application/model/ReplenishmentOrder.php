<?php
/**
 * Created by PhpStorm.
 * User: liyu
 * Date: 2018/1/30
 * Time: 上午11:49
 */

namespace app\model;


class ReplenishmentOrder extends Base
{
    protected $autoWriteTimestamp = 'datetime';

    public static function getListByWhere($where)
    {
        return self::all(function($query)use($where){
            return $query->where($where);
        });
    }
}
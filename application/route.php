<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
use think\Route;

//后台系统
Route::post(':version/importTables','admin/:version.Tables/loadTables'); //导入报表
Route::get(':version/exportTables','admin/:version.Tables/exportTables'); //导出报表
Route::post(':version/createPartnerDayProfitCount','admin/:version.Tables/createPartnerDayProfitCount'); //生成合伙人日收入明细

Route::post(':version/uploadImg','admin/:version.File/uploadImg'); //上传图片
Route::post(':version/addUser','admin/:version.User/addUser'); //添加用户
Route::post(':version/offUser','admin/:version.User/offUser'); //冻结用户
Route::post(':version/onUser','admin/:version.User/onUser'); //解冻用户
Route::post(':version/updateUser','admin/:version.User/updateUser'); //修改用户
Route::get(':version/getUserList','admin/:version.User/getUserList'); //获取用户列表（分页）

//微信h5
Route::post(':version/partner/login','mobile/:version.User/login'); //管家登陆
Route::get(':version/partner/getUserInfo','mobile/:version.User/getUserInfo'); //个人信息
Route::get(':version/partner/getUserProfit','mobile/:version.User/getUserProfit'); //收入
Route::get(':version/partner/getUserProfitDetail','mobile/:version.User/getUserProfitDetail'); //收入详情

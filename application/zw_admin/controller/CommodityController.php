<?php
namespace app\zw_admin\controller;
use \think\Controller;
use think\Db;
// use think\Controller;
use think\Session;
use think\Request;
use think\File;
// use app\zw_admin\controller\ComController;
class CommodityController extends ComController
{
	//商品入库
    public function add()
    {
        if (Request::instance()->isPost()){
            //获取表单其他信息
            $list = input('post.');
            
            // dump(input('post.'));
            $file = request()->file('img');
            // dump($file);
            // 移动到框架应用根目录/public/uploads/ 目录下
            if($file){
                $info = $file->move(ROOT_PATH . 'public' . DS . 'uploads');
                if($info){
                    //获取图片路径
                    $list['img'] =  'uploads/'.$info->getSaveName();
                }else{
                    // 上传失败获取错误信息
                    echo $file->getError();
                }
            }
            unset($list['demo3']);
            $list['number'] = input('post.demo3');
            //将数据添加至数据库
            if (db('commodity')->insert($list) > 0) {
                return $this->success('添加成功');
            }else{
                return $this->error('添加失败');
            }
           
        }else{
            $list = Db::name('commodity')->where(['pid'=>'0'])->select();
            $this->assign('list',$list);
            return $this->fetch('add');
        }

        
    }


    /**
     * 种类添加
     * @return [type] [description]
     */
    public function class_add()
    {

        $class_list = Db::name('class_goods')->where(['pid'=>'0'])->select();
        $this->assign('list',$class_list);
        return $this->fetch('class_add');
    }

    //添加
    public function class_addto()
    {
        // echo '111';
        // exit;
        if (Request::instance()->isPost()){
            // dump(input('post.'));
            $list = input('post.');
            $file = request()->file('img');
            // dump($file);
            // 移动到框架应用根目录/public/uploads/ 目录下
            if($file){
                $info = $file->move(ROOT_PATH . 'public' . DS . 'uploads');
                if($info){
                    //获取图片路径
                    $list['img'] =  'uploads/'.$info->getSaveName();
                }else{
                    // 上传失败获取错误信息
                    echo $file->getError();
                }
            }

            if (db('class_goods')->insert($list) > 0) {
                return $this->success('添加成功');
            }else{
                return $this->error('添加失败');
            }
        }else{
            // dump(input('param.id'));
            // exit;
            $pid = input('param.id');
            $class_list = Db::name('class_goods')->where(['pid'=>'0'])->select();
            $p_class = Db::name('class_goods')->where(['id'=>$pid])->find();
            $p_name = $p_class['class_name'];
            $this->assign('list',$class_list);
            $this->assign('pid',$pid);
            $this->assign('p_name',$p_name);
            return $this->fetch('class_addto');
        }
    }



    /**
     * 查看子类列表
     * @return [type] [description]
     */
    public function class_index()
    {
        // dump(input('param.pid'));
        // exit;
        $pid = input('param.pid');
        $class_list = Db::name('class_goods')->where(['pid'=>$pid])->select();
        $class_name = Db::name('class_goods')->where(['id'=>$pid])->find();
        $this->assign('list',$class_list);
        $this->assign('class_name',$class_name['class_name']);
        return $this->fetch('class_index');

    }

    /**
     * 编辑种类
     * @return [type] [description]
     */
    public function class_edit()
    {


        if (Request::instance()->isPost()){
            // dump(input('post.'));
            // exit;
            $id = input('post.id');
            $list = input('post.');
            $file = request()->file('img');
            if (empty($file)) {
                $list['img'] = input('post.img_tow');
                unset($list['img_tow']);
                

            }else{
                if($file){
                    $info = $file->move(ROOT_PATH . 'public' . DS . 'uploads');
                    if($info){
                        //获取图片路径
                        $list['img'] =  'uploads/'.$info->getSaveName();
                        unset($list['img_tow']);
                    }else{
                        // 上传失败获取错误信息
                        echo $file->getError();
                    }
                }
            }

            // unset($list['id']);
            // dump($id);
            // dump($list);
            // exit;
            // 移动到框架应用根目录/public/uploads/ 目录下
            

            if (db('class_goods')->where(['id'=>$id])->update($list) > 0) {
                return $this->success('编辑成功');
            }else{
                return $this->error('编辑失败');
            }
        }else{
            $id = input('param.id');
            $class_list = Db::name('class_goods')->where(['id'=>$id])->select();

            if ($class_list[0]['pid'] == '0') {
                $this->assign('list',$class_list);
            }else{
                $class_arr = Db::name('class_goods')->where(['id'=>$class_list[0]['pid']])->find();

                foreach ($class_list as $key => $value) {
                        $clist = $value;
                        $clist['pname'] = $class_arr['class_name'];
                        // $clist['pid'] = $class_arr['class_name'];
                        $carr[] = $clist;    
                }

                // dump($carr);
                // exit;
                $this->assign('list',$carr);
            }
            $this->assign('id',$id);
            return $this->fetch('class_edit');
        }
        
       
    }

    /**
     * 删除种类
     * @return [type] [description]
     */
    public function del()
    {   
        $id = input('post.id');
        $del_count = Db::name('class_goods')->where('id',$id)->delete();
        if ($del_count > 0) {
            return '111';
        }else{
            return '222';
        }
        // dump(input('post.id'));
        // exit;
    }

    /**
     * 品牌列表
     * @return [type] [description]
     */
    public function brand_index()
    {
        // $brand_list = Db::name('brand')->select();
       $brand_list = Db::name('brand')
        ->alias('b')
        ->join('drm_class_goods w','b.class_id = w.id')
        ->select();
        // dump($brand_list);
        // exit;
        $this->assign('list',$brand_list);
        return $this->fetch('brand_index');
    }

    /**
     * 品牌添加
     * @return [type] [description]
     */
    public function brand_add()
    {
        if (Request::instance()->isPost()){
            // dump(input('post.'));
            $list = input('post.');
            $file = request()->file('img');
            // dump($file);
            // 移动到框架应用根目录/public/uploads/ 目录下
            if($file){
                $info = $file->move(ROOT_PATH . 'public' . DS . 'uploads');
                if($info){
                    //获取图片路径
                    $list['brand_img'] =  'uploads/'.$info->getSaveName();
                }else{
                    // 上传失败获取错误信息
                    echo $file->getError();
                }
            }
            // dump($list);
            // exit;
            if (db('brand')->insert($list) > 0) {
                return $this->success('添加成功');
            }else{
                return $this->error('添加失败');
            }
        }else{
            // dump(input('param.id'));
            // exit;
          
            $class_list = Db::name('class_goods')->where(['pid'=>'0'])->select();
            $selling_list = Db::name('selling')->select();
            
            $this->assign('list',$class_list);
            $this->assign('selling_list',$selling_list);
            
            return $this->fetch('brand_add');
        }
    }

   
    /**
     * 品牌编辑
     * @return [type] [description]
     */
   public function brand_edit()
   {
        if (Request::instance()->isPost()){
            $id = input('post.id');
            $list = input('post.');

            $file = request()->file('img');
            if (empty($file)) {
                $list['brand_img'] = input('post.img_tow');
                unset($list['img_tow']);
                

            }else{
                if($file){
                    $info = $file->move(ROOT_PATH . 'public' . DS . 'uploads');
                    if($info){
                        //获取图片路径
                        $list['brand_img'] =  'uploads/'.$info->getSaveName();
                        unset($list['img_tow']);
                    }else{
                        // 上传失败获取错误信息
                        echo $file->getError();
                    }
                }
            }
            unset($list['id']);
            // dump($list);
            // exit;
            if (db('brand')->where(['brand_id'=>$id])->update($list) > 0) {
                return $this->success('添加成功');
            }else{
                return $this->error('添加失败');
            }
        }else{
            $id = input('param.id');
            $brand_list = Db::name('brand')->where(['brand_id'=>$id])->select();
            // dump($brand_list);
            // exit;
            $class_list = Db::name('class_goods')->where(['pid'=>'0'])->select();
            $class_arr = Db::name('class_goods')->where(['id'=>$brand_list[0]['class_id']])->find();
            $this->assign('list',$brand_list);
            $this->assign('class_list',$class_list);
            $this->assign('class_name',$class_arr['class_name']);
            return $this->fetch('brand_edit');
        }

       
   }



   /**
    * 品牌删除
    * @return [type] [description]
    */
   public function brand_del()
   {
        $id = input('post.id');
        $del_count = Db::name('brand')->where('brand_id',$id)->delete();
        if ($del_count > 0) {
            return '111';
        }else{
            return '222';
        }
   }

    //展示商品
    public function index()
    {
        $list = Db::name('commodity')->where(['pid'=>'0'])->select();
        $this->assign('list',$list);
        // return input('post.name');
        return $this->fetch('index');
    }


    /**
     * 展示商品分类
     */

    public function paging()
    {
        // dump(input('get.'));
        $id = input('get.id');
        $list = Db::name('commodity')->where(['pid' => $id])->select();
        $plist = Db::name('commodity')->where(['id' => $id])->find();
        $title = $plist['name'];
        $this->assign('list',$list);
        $this->assign('title',$title);
        return $this->fetch('paging');
    }

    /**
     * 热销商品展示
     */
    public function selling()
    {
        $list = Db::name('commodity')->where('pid','neq','0')->order('sales desc')->select();
        // dump($list);
        // exit;
        $this->assign('list',$list);
        return $this->fetch('selling');
    }


    /**
     * 商品回收站
     */
    
    public function recycle()
    {
        if (Request::instance()->isPost()){
            $id = input('post.id');
            $list = Db::name('commodity')->where(['id'=>$id])->select();
            $arr = $list[0];
            if (!empty($list)) {
                $arr['time'] = date('Y-m-d H:i');
                if (db('commodity')->where('id',$id)->delete() > 0) {
                    if (db('recycle')->insert($arr) > 0) {
                        return '1111';
                    }else{
                        return '2222';
                    }
                }else{
                    return '2222';
                }
                // dump($arr);
                // exit;
                
            }else{
                return '2222';
            }
        }else{
            // dump(input('post.'));
            // exit;
            $list = Db::name('recycle')->order('time desc')->select();
            // dump($list);
            // exit;
            $this->assign('list',$list);
            return $this->fetch('recycle');
        }
        
    }


}

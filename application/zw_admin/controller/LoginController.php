<?php
namespace app\zw_admin\controller;
use think\Controller;
use think\Request;
use think\Db;
use think\Session;
class LoginController extends Controller
{
	//登入判断
    public function login()
    {
    	if (Request::instance()->isPost()){
    		$uname = input('post.uname');
    		$pass = md5(md5(input('post.password')));
    		// echo $uname.$pass;
    		$list = Db::name('user')->where(['user'=>$uname,'pass'=>$pass])->select();
    		if (empty($list)) {
    			// echo '111';
    			return $this->error('登入失败');
    		}else{
    			Session::set('uid',$list[0]['id']);
    			// dump($_SESSION);
       //          exit;
    			return $this->redirect('Index/index');
    		}
    	}else{
    		return $this->fetch('login');
    	}
        
     }
}
<?php
namespace app\zw_admin\controller;
use \think\Controller;
use think\Db;
// use think\Controller;
use think\Session;
use think\Request;
// use app\zw_admin\controller\ComController;
class MerchantController extends ComController
{
	/**
     * 商户信息展示
     * @return [type] [description]
     */
    public function index()
    {
        // $s_count =  Db::name('commodity')->where('pid','neq','0')->count();
        // // dump($s_number);
        // // exit;
        // $list = Db::name('merchant')->select();
        $list = Db::name('merchant')
        ->alias('b')
        ->join('drm_selling w','b.m_selling = w.selling_id')
        ->select();
        // dump($list);
        // exit;
        $this->assign('list',$list);
        return $this->fetch('index');
    }


   
    /**
     * 商户信息添加
     */
   public function add()
   {
        if (Request::instance()->isPost()){
           
           // dump(input('post.'));
           $list = input('post.');
           $m_brand = implode(",",$list['m_brand']);
           unset($list['m_brand']);
           $list['m_brand'] = $m_brand;
           // dump($list);
           if (db('merchant')->insert($list) > 0) {
                return $this->success('添加成功');
            }else{
                return $this->error('添加失败');
            }
        }else{
            $brand_list = Db::name('brand')->select();
            $selling_list = Db::name('selling')->select();
            $s_province = Db::name('selling')->distinct(true)->field('s_province')->select();
            // dump($di);
            // exit;
            $this->assign('class_list',$brand_list);
            $this->assign('selling_list',$selling_list);
            $this->assign('s_province',$s_province);
            return $this->fetch('add');
        }
   }



   public function selling_ajax()
   {
        // dump(input('post.'));
        $province = input('post.province');
        $selling_list = Db::name('selling')->where(['s_province'=>$province])->select();
        // dump($selling_list);
        return $selling_list;
   }




   /**
    * 商户账号信息列表
    * @return [type] [description]
    */
   public function seller()
   {    
        $seller_list = Db::name('seller')
        ->alias('b')
        ->join('drm_merchant w','b.merchant_name = w.m_id')
        ->select();

        // dump($seller_list);
        // exit;
        $this->assign('seller_list',$seller_list);
        return $this->fetch();
   }




   /**
    * 商户账号添加
    * @return [type] [description]
    */
   public function seller_add()
   {
        if (Request::instance()->isPost()){
           
           // dump(input('post.'));
           $list = input('post.');
           if ($list['pwd'] != $list['pwd_to']) {
               return $this->error('两次密码不一样,请重新输入!');
               exit;
           }


           unset($list['pwd_to']);
           $list['pwd'] = md5(md5($list['pwd']));
           // dump($list);
           // exit;
           if (db('seller')->insert($list) > 0) {
                return $this->success('添加成功');
            }else{
                return $this->error('添加失败');
            }
          
        }else{
            $brand_list = Db::name('brand')->select();
            // $selling_list = Db::name('selling')->select();
            $s_province = Db::name('merchant')->distinct(true)->field('s_province')->select();
            // dump($di);
            // exit;
            $this->assign('class_list',$brand_list);
            // $this->assign('selling_list',$selling_list);
            $this->assign('s_province',$s_province);
            return $this->fetch('seller_add');
        }
   }




   public function merchant_ajax()
   {
        // dump(input('post.'));
        $province = input('post.province');
        $merchant_list = Db::name('merchant')->where(['s_province'=>$province])->select();
        // dump($selling_list);
        return $merchant_list;
   }

}

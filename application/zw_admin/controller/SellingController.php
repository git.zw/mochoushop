<?php
namespace app\zw_admin\controller;
use \think\Controller;
use think\Db;
// use think\Controller;
use think\Session;
use think\Request;
use think\File;
// use app\zw_admin\controller\ComController;
class SellingController extends ComController
{
    /**
     * 卖场信息
     * @return [type] [description]
     */
    public function index()
    {
        $list = Db::name('selling')->select();
            // dump($list);
            // exit;
        $this->assign('list',$list);
        return $this->fetch('index');
    }


    /**
     * 卖场信息添加
     */
    public function add()
    {

        if (Request::instance()->isPost()){
            //获取表单其他信息
            $list = input('post.');
            $brand_list = implode(",",$list['brand_name']);
            $brand_id = $list['brand_name'];
            $list['b_id'] = $brand_list;
            unset($list['brand_name']);
           
            foreach ($brand_id as $key => $value) {
                $er['brand_id'] = $value;
                $arr[] = $er;
            }
            dump($arr);
            exit;
            // dump(input('post.'));
            // 
            $selling_id = Db::name('selling')->insertGetId($list);
            // echo $selling_id;
            // exit;
            foreach ($arr as $key => $value) {
                // $res = $value;
                // $res['selling_id'] = $selling_id;
                $value['selling_id'] = $selling_id;
                $res [] = $value;

            }
            // dump($res);
            // exit;

            if (db('selling_brand')->insertAll($res) > 0) {
                return $this->success('添加成功');
            }else{
                return $this->error('添加失败');
            }
           
        }else{

            $list = Db::name('brand')->select();
            // dump($list);
            // exit;
            $this->assign('list',$list);
            return $this->fetch('add');
        }
        
    }

}

/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50553
Source Host           : localhost:3306
Source Database       : mochoushop

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2018-07-13 17:31:32
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for drm_brand
-- ----------------------------
DROP TABLE IF EXISTS `drm_brand`;
CREATE TABLE `drm_brand` (
  `brand_id` int(10) NOT NULL AUTO_INCREMENT,
  `brand_name` varchar(255) DEFAULT NULL,
  `brand_img` varchar(255) DEFAULT NULL,
  `class_id` int(10) DEFAULT NULL,
  `type` int(10) NOT NULL DEFAULT '0',
  `s_id` int(10) DEFAULT NULL COMMENT '卖场id',
  PRIMARY KEY (`brand_id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of drm_brand
-- ----------------------------
INSERT INTO `drm_brand` VALUES ('4', '九牧', 'uploads/20180619\\73a826adcf158669e3590f858a43392c.png', '4', '0', null);
INSERT INTO `drm_brand` VALUES ('3', '简乐家居', 'uploads/20180619\\1e6f67499a9f08dea3abede6b32f01c3.jpg', '1', '0', null);
INSERT INTO `drm_brand` VALUES ('5', '美学家具', 'uploads/20180622\\b02976c9ba7c76ee92898bc08ccc0ef5.jpg', '1', '0', null);
INSERT INTO `drm_brand` VALUES ('6', '罗兰圣帝瓷砖', 'uploads/20180622\\ff578f32b0fe71d60e09a60fd42fc810.jpg', '8', '0', null);
INSERT INTO `drm_brand` VALUES ('7', '雷士照明', 'uploads/20180622\\0f28341452131861be81ff6caac58799.jpg', '13', '0', null);
INSERT INTO `drm_brand` VALUES ('8', '康尔橱柜', 'uploads/20180622\\bddc6d7ba86dbde2d5156ba5ad004cc6.jpg', '7', '0', null);
INSERT INTO `drm_brand` VALUES ('9', '铂芙艺术壁纸', 'uploads/20180622\\a2682e76d3187f4610ed15cf60826f3b.jpg', '12', '0', null);
INSERT INTO `drm_brand` VALUES ('10', '鑫凯斯木门', 'uploads/20180622\\b8b983fe17d71e3669382dffc27a4bac.jpg', '10', '0', null);
INSERT INTO `drm_brand` VALUES ('11', '思尔诺地板', 'uploads/20180622\\6a0497c95149e3a0881188b832ea07f1.jpg', '9', '0', null);

-- ----------------------------
-- Table structure for drm_class_goods
-- ----------------------------
DROP TABLE IF EXISTS `drm_class_goods`;
CREATE TABLE `drm_class_goods` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `class_name` varchar(200) DEFAULT NULL,
  `pid` int(10) NOT NULL DEFAULT '0' COMMENT '父级id',
  `img` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of drm_class_goods
-- ----------------------------
INSERT INTO `drm_class_goods` VALUES ('1', '家具', '0', 'uploads/20180619\\05e04a562a53a90ccbc552e3e7994f3a.jpg');
INSERT INTO `drm_class_goods` VALUES ('4', '卫浴', '0', 'uploads/20180615\\7dee6f45e333694e5fa41776413e9d9d.jpg');
INSERT INTO `drm_class_goods` VALUES ('5', '床', '1', 'uploads/20180619\\ba1f6e859cfd6042b3ea8c64a6ca0b5e.png');
INSERT INTO `drm_class_goods` VALUES ('6', '衣柜', '0', 'uploads/20180622\\fc3627ed817696f9427226abcf2bab02.jpg');
INSERT INTO `drm_class_goods` VALUES ('7', '橱柜', '0', 'uploads/20180622\\3d4b24696d59d970bb0c1d188dad20ce.jpg');
INSERT INTO `drm_class_goods` VALUES ('8', '瓷砖', '0', 'uploads/20180622\\d2850a10c528822a7188aefdecd32896.jpg');
INSERT INTO `drm_class_goods` VALUES ('9', '地板', '0', 'uploads/20180622\\3349ad3365386ca5433493348ea0bd44.jpg');
INSERT INTO `drm_class_goods` VALUES ('10', '门窗', '0', 'uploads/20180622\\476995c37d71fd2fd793b5a5ff4ca56d.jpg');
INSERT INTO `drm_class_goods` VALUES ('11', '窗帘', '0', 'uploads/20180622\\85f83e350cc59030730b262dc7f7d82d.jpg');
INSERT INTO `drm_class_goods` VALUES ('12', '墙纸', '0', 'uploads/20180622\\d8bdc23d5db181a0ea9944d8a4f66d70.jpg');
INSERT INTO `drm_class_goods` VALUES ('13', '灯饰', '0', 'uploads/20180622\\4e771a06b380b3da9778a5a8a731a0e0.jpg');

-- ----------------------------
-- Table structure for drm_commodity
-- ----------------------------
DROP TABLE IF EXISTS `drm_commodity`;
CREATE TABLE `drm_commodity` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` char(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '商品名称',
  `com` char(20) CHARACTER SET utf8 DEFAULT NULL COMMENT '商品单位',
  `number` int(10) DEFAULT NULL COMMENT '商品数量',
  `img` char(100) DEFAULT NULL COMMENT '商品图片',
  `unit` float(10,2) DEFAULT NULL COMMENT '商品价格',
  `content` text CHARACTER SET utf8 COMMENT '简介',
  `cid` int(10) DEFAULT NULL,
  `pid` int(10) DEFAULT NULL COMMENT '父级id',
  `off` int(10) NOT NULL DEFAULT '0' COMMENT '是否上架',
  `sales` int(10) NOT NULL DEFAULT '0' COMMENT '销量',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of drm_commodity
-- ----------------------------
INSERT INTO `drm_commodity` VALUES ('14', '床', '', '1', 'uploads/20180613\\023622c6d0541a8cac0819ffa3585324.jpg', null, '', null, '0', '0', '0');

-- ----------------------------
-- Table structure for drm_goods_order
-- ----------------------------
DROP TABLE IF EXISTS `drm_goods_order`;
CREATE TABLE `drm_goods_order` (
  `order_id` int(10) NOT NULL AUTO_INCREMENT,
  `number` varchar(200) DEFAULT NULL COMMENT '订单编号',
  `class_goods_id` int(10) DEFAULT NULL COMMENT '商品类别id',
  `merchant_name` varchar(200) DEFAULT NULL COMMENT '商户名称',
  `b_id` int(10) DEFAULT NULL COMMENT '品牌id',
  `m_phone` int(11) DEFAULT NULL COMMENT '卖场电话',
  `d_phone` int(11) DEFAULT NULL COMMENT '订单员电话',
  `maddress` varchar(255) DEFAULT NULL COMMENT '卖场地址',
  `user_name` varchar(200) DEFAULT NULL COMMENT '收货人',
  `user_phone` int(11) DEFAULT NULL COMMENT '收货人联系电话',
  `user_one_phone` int(11) DEFAULT NULL COMMENT '客户电话',
  `user_address` varchar(255) DEFAULT NULL COMMENT '收货人地址',
  `goods_id` int(10) DEFAULT NULL COMMENT '商品id',
  `spec` varchar(100) DEFAULT NULL COMMENT '规格',
  `com` varchar(100) DEFAULT NULL COMMENT '单位',
  `num` int(10) DEFAULT NULL COMMENT '数量',
  `unit` int(10) DEFAULT NULL COMMENT '金额',
  `remarks` varchar(255) DEFAULT NULL COMMENT '备注',
  `time` varchar(100) DEFAULT NULL COMMENT '订单时间',
  `total` int(10) DEFAULT NULL COMMENT '总金额',
  PRIMARY KEY (`order_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of drm_goods_order
-- ----------------------------

-- ----------------------------
-- Table structure for drm_member
-- ----------------------------
DROP TABLE IF EXISTS `drm_member`;
CREATE TABLE `drm_member` (
  `uid` int(10) NOT NULL AUTO_INCREMENT,
  `phone` varchar(11) DEFAULT NULL,
  `pwd` varchar(255) DEFAULT NULL,
  `time` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of drm_member
-- ----------------------------
INSERT INTO `drm_member` VALUES ('2', '17621223542', '24e18d27e4cd9b0b9dc2b035128d64cc', '2018-07-09 15:11');

-- ----------------------------
-- Table structure for drm_merchant
-- ----------------------------
DROP TABLE IF EXISTS `drm_merchant`;
CREATE TABLE `drm_merchant` (
  `m_id` int(10) NOT NULL AUTO_INCREMENT,
  `m_name` varchar(255) DEFAULT NULL,
  `m_brand` varchar(255) DEFAULT NULL,
  `m_selling` int(20) DEFAULT NULL,
  `s_province` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`m_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of drm_merchant
-- ----------------------------
INSERT INTO `drm_merchant` VALUES ('1', '测试商户名称', '3,5', '10', '北京');

-- ----------------------------
-- Table structure for drm_recycle
-- ----------------------------
DROP TABLE IF EXISTS `drm_recycle`;
CREATE TABLE `drm_recycle` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` char(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '商品名称',
  `com` char(20) CHARACTER SET utf8 DEFAULT NULL COMMENT '商品单位',
  `number` int(10) DEFAULT NULL COMMENT '商品数量',
  `img` char(100) DEFAULT NULL COMMENT '商品图片',
  `unit` float(10,2) DEFAULT NULL COMMENT '商品价格',
  `content` text CHARACTER SET utf8 COMMENT '简介',
  `cid` int(10) DEFAULT NULL,
  `pid` int(10) DEFAULT NULL COMMENT '父级id',
  `off` int(10) NOT NULL DEFAULT '0' COMMENT '是否上架',
  `sales` int(10) NOT NULL DEFAULT '0' COMMENT '销量',
  `time` char(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of drm_recycle
-- ----------------------------
INSERT INTO `drm_recycle` VALUES ('7', '长寿果袋装210g', '袋', '100', 'uploads/20180206/fd09ecc855c09452c0190d5d1c614e9a.jpg', '17.00', '碧根果/长寿果袋装210g', null, '4', '0', '0', '2018-02-07 17:22');

-- ----------------------------
-- Table structure for drm_seller
-- ----------------------------
DROP TABLE IF EXISTS `drm_seller`;
CREATE TABLE `drm_seller` (
  `seller` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `pwd` varchar(255) DEFAULT NULL,
  `merchant_name` varchar(255) DEFAULT NULL COMMENT '所属商户',
  `phone` varchar(255) DEFAULT NULL,
  `s_province` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`seller`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of drm_seller
-- ----------------------------
INSERT INTO `drm_seller` VALUES ('1', 'OLY123456', '14e1b600b1fd579f47433b88e8d85291', '1', '17621223542', '北京');

-- ----------------------------
-- Table structure for drm_selling
-- ----------------------------
DROP TABLE IF EXISTS `drm_selling`;
CREATE TABLE `drm_selling` (
  `selling_id` int(10) NOT NULL AUTO_INCREMENT,
  `selling_name` varchar(255) DEFAULT NULL,
  `s_province` varchar(200) DEFAULT NULL COMMENT '省',
  `s_city` varchar(200) DEFAULT NULL COMMENT '市',
  `s_county` varchar(200) DEFAULT NULL COMMENT '县',
  `address` varchar(255) DEFAULT NULL COMMENT '详细地址',
  `b_id` varchar(200) DEFAULT NULL COMMENT '品牌id',
  `route` varchar(255) DEFAULT NULL COMMENT '路线',
  PRIMARY KEY (`selling_id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of drm_selling
-- ----------------------------
INSERT INTO `drm_selling` VALUES ('10', '测试卖场名称1', '北京', '市辖区', '东城区', '建设大道737号广发银行大厦', '4,3,6', '102路;530路外环;540路;566路;577路;583路;618路;702路;723路;724路;805路;电7路;电8路');
INSERT INTO `drm_selling` VALUES ('11', '我是卖场名称2', '湖北', '武汉市', '江汉区', '台北一路宝岛社区', '7,9,10', '地铁3号线宝岛公园站下车，公交11路线、21路');
INSERT INTO `drm_selling` VALUES ('12', '我是卖场信息2', '湖北', '武汉市', '江汉区', '台北三路台北三村', '4,3,5', '地铁3号线宝岛公园站下车，公交11路线、21路');

-- ----------------------------
-- Table structure for drm_selling_brand
-- ----------------------------
DROP TABLE IF EXISTS `drm_selling_brand`;
CREATE TABLE `drm_selling_brand` (
  `sb_id` int(10) NOT NULL AUTO_INCREMENT,
  `selling_id` int(10) DEFAULT NULL,
  `brand_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`sb_id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of drm_selling_brand
-- ----------------------------
INSERT INTO `drm_selling_brand` VALUES ('3', '10', '4');
INSERT INTO `drm_selling_brand` VALUES ('6', '11', '7');
INSERT INTO `drm_selling_brand` VALUES ('7', '11', '9');
INSERT INTO `drm_selling_brand` VALUES ('8', '11', '10');
INSERT INTO `drm_selling_brand` VALUES ('9', '12', '4');
INSERT INTO `drm_selling_brand` VALUES ('10', '12', '3');
INSERT INTO `drm_selling_brand` VALUES ('11', '12', '5');

-- ----------------------------
-- Table structure for drm_user
-- ----------------------------
DROP TABLE IF EXISTS `drm_user`;
CREATE TABLE `drm_user` (
  `id` int(10) NOT NULL,
  `user` char(50) NOT NULL,
  `pass` char(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of drm_user
-- ----------------------------
INSERT INTO `drm_user` VALUES ('1', 'admin', '14e1b600b1fd579f47433b88e8d85291');

-- ----------------------------
-- Table structure for drm_verification
-- ----------------------------
DROP TABLE IF EXISTS `drm_verification`;
CREATE TABLE `drm_verification` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `phone` varchar(200) DEFAULT NULL,
  `vas` varchar(200) DEFAULT NULL,
  `time` varchar(200) DEFAULT NULL,
  `num` int(10) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of drm_verification
-- ----------------------------
INSERT INTO `drm_verification` VALUES ('3', '17621223542', '4841', '2018-07-09 15:11', '2', '::1');
